const audio = document.createElement("audio");
audio.src = './audio/ees2.mp3';
audio.type = 'audio/mpeg';
audio.loop = true;
let isAudioReady = false;

const infoTrack = document.getElementById('infoTrack')
infoTrack.textContent = 'Audio is charging...'

const waves = document.querySelectorAll('.antena-animation>span')

const playerCtrls = document.querySelectorAll('.player-ctrl')
const speakers = {
    left: document.querySelector('.speaker-L'),
    rigth: document.querySelector('.speaker-R')
};

/**
 * Speakers and Antenna Animation, and sound play/pause/stop functions.
 */
playerCtrls.forEach((pc, idx) => {
    pc.addEventListener('click', () => {
        if (isAudioReady) {
            reset()
            
            // idx: 1 => play was clicked
            idx === 1 ? speakersAnimate() : stopAnim(idx);

            // idx: 0 => stop, so if pause or play were clicked
            if (idx !== 0) {
                pc.classList.toggle('active')
                pc.classList.contains('active') ?
                    pc.style.setProperty(`--btn${idx + 1}`, .5 + "em") :
                    pc.style.setProperty(`--btn${idx + 1}`, 1 + "em");
            }
        }
    })
});
function reset() {
    playerCtrls.forEach((pc, idx) => {
        pc.classList.remove('active')
        pc.style.setProperty(`--btn${idx + 1}`, 1 + "em")
    })
};
function speakersAnimate() {
    infoTrack.textContent = 'Play';
    audio.play()
    speakers.left.classList.toggle('animate', true)
    speakers.rigth.classList.toggle('animate', true)

    waves.forEach(w => {
        w.style.animationName = 'antenaWaves';
    })
};
function stopAnim(idx) {
    if (idx === 0) {
        audio.pause()
        audio.currentTime = 0
    } else {
        infoTrack.textContent = 'Pause';
        audio.pause()
    }
    speakers.left.classList.toggle('animate', false)
    speakers.rigth.classList.toggle('animate', false)

    waves.forEach(w => {
        w.style.animationName = '';
    })
};

/**
 * Mouve the tape player (mousedown and mousemouve),
 * on the X and Y axis.
 */
const tapePlayer = document.querySelector('.tape-player')
const bodyDiv = document.querySelector('.body')

bodyDiv.addEventListener("mousedown", () => {
    bodyDiv.onmousemove = (e) => {
        mouseMoveFunction(e);
    }
});
bodyDiv.addEventListener("mouseup", function (e) {
    bodyDiv.onmousemove = null
});
let angleX = 0;
let angleY = 0;
function mouseMoveFunction(e) {
    // X axis
    angleX = e.clientX / 10 - 90
    tapePlayer.style.transform = 'rotateY(' + angleX + 'deg)';

    // Y axis
    angleY = e.clientY / 10 - 60
    bodyDiv.style.perspectiveOrigin = '50% ' + angleY + 'em';
}


/**
 * Volume Input
 */
const audioVol = document.getElementById('vol')
audioVol.oninput = () => {
    audio.volume = audioVol.value;
}


audio.addEventListener('canplaythrough', () => {
    console.log('audio is ready !');
    isAudioReady = true;
    infoTrack.textContent = 'Hold the click to move the player !'
}, false);